import job from "./models/job.js";
import { todoListService } from "./services/listTodoService.js";
let toDoList = [];

let addItem = () => {
    let newTask = document.getElementById("newTask").value;
    document.getElementById("newTask").value = "";
    let todo = new job(newTask);
    console.log(todo);
    todoListService.addToDo(todo).then(() => {
        loadToDoList();
    }).catch((error) => {
        console.log(error);
    })
};
window.addItem = addItem;


let removeItem = (id) => {
    todoListService.deleteToDo(id).then(() => {
        loadToDoList();
    }).catch((error) => {
        console.log(error);
    })
}
window.removeItem = removeItem;

let renderToDoList = (toDoList) => {
    let contentHTML = "";
    for (let i = 0; i < toDoList.length; i++) {
        let todo = toDoList[i];
        let contentTodo = `<li> ${todo.content}
                                    <div>
                                        <a class="buttons">
                                            <button aria-label="add" class="remove" onclick="removeItem(${todo.id})">
                                                <i class="fa fa-trash-alt"></i>
                                            </button>
                                        </a>
                                        <a class="buttons">
                                            <button aria-label="add" class="complete"  onclick="complete()">
                                            <span> <i class="fa fa-check-circle"></i></span>
                                            </button>
                                        </a>
                                    </div>
                                </li>`;
        contentHTML += contentTodo;

    }
    document.getElementById("todo").innerHTML = contentHTML;
};


let loadToDoList = () => {
    todoListService.getListToDo().then((res) => {
        toDoList = res.data;
        renderToDoList(toDoList);
    }).catch((error) => {
        console.log(error);
    })
}
loadToDoList();