export let todoListService = {
    getListToDo: () => {
        return axios({
            url: "https://62b740320d4a2cd3e1a96fea.mockapi.io/todo-list",
            method: "GET",
        });
    },
    addToDo: (todo) => {
        return axios({
            url: "https://62b740320d4a2cd3e1a96fea.mockapi.io/todo-list",
            method: "POST",
            data: todo,
        })
    },
    deleteToDo: (idToDo) => {
        return axios({
            url: `https://62b740320d4a2cd3e1a96fea.mockapi.io/todo-list/${idToDo}`,
            method: "DELETE",
        });
    },
};